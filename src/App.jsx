import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import { useState } from "react"
import Navbar from "./scenes/global/Navbar"
import Home from "./scenes/home/Home"
import NotFound from "./scenes/notfound/NotFound"
import "bootstrap/dist/css/bootstrap.min.css"
import { useGlobalContext } from "./state/index"
import CartModal from "./scenes/global/CartModal"
import ShoppingList from "./scenes/home/ShoppingList"
import CreateUpdateProductModal from "./scenes/home/CreateUpdateProductModal"
import ItemPage from "./scenes/home/ItemPage"
import OrderPage from "./scenes/home/OrderPage"
import AdminDashBoard from "./scenes/home/AdminDashBoard"



function App() {  
  const { showModal, showModalProduct, user } = useGlobalContext()
  const [selectedPage, setSelectedPage, ] = useState("home");



  return (      
    <div className="app">
      <Router>
        <Navbar 
          selectedPage={selectedPage} 
          setSelectedPage={setSelectedPage}
        />
        {showModal && <CartModal />}
        {showModalProduct && <CreateUpdateProductModal />}
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/products" element={<ShoppingList />} />
          <Route exact path="/item" element={<ItemPage />} />
          <Route exact path="/order" element={<OrderPage />} />
          {user.isAdmin && <Route exact path="/admin" element={<AdminDashBoard />} />}
          <Route path="/*" element={<NotFound />} />
        </Routes>        
      </Router>
    </div>
  )
}

export default App
