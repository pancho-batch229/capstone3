import { Card, Button } from "react-bootstrap"
import { Link, useNavigate } from "react-router-dom" 
import { useState, useEffect } from "react"
import Swal from "sweetalert2"
import { useGlobalContext } from "../state/index"


const Item = ({ productProps }) => {
	const { _id , name, description, price } = productProps
	const [ isDisabled, setIsDisabled ] = useState(true)
	const { user, isLoggedIn, setSelectedProductId, setCartProducts, cartProducts } = useGlobalContext() 
	const navigate = useNavigate()

	useEffect(() => {
		if(!isLoggedIn) {
			setIsDisabled(true)
		} else {
			setIsDisabled(false)
		}

	}, [isLoggedIn])

	const handleImageClicked = () => {
		setSelectedProductId(_id)
		navigate('/item')
	}

	const addToCart = async () => {
		const response = await fetch(`https://capston2-mangco.onrender.com/carts/${user.id}`, {
			method: "POST",
			body: JSON.stringify({
				productId: _id,
				quantity: 1,
			}),
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			}
		})

		const data = await response.json()
		Swal.fire({
			position: 'top-end',
			timer: 1500,
			showConfirmButton: false,
			color: '#8f7840',
			background: '#ecebeb',
	    	title: "Success",
	    	icon: "success",
	    	text: `${name} successfully added to cart` 
	    })
	}

	return (
		<div className="card-container">
			<Card className="cardHighlight">
				<Card.Img className="product-image" onClick={handleImageClicked} variant="top" src={`../assets/products/${_id}.png`} />
				<Card.Body className="d-flex flex-column card-body">
					<div>
						<Card.Title className="pb-2">{name}</Card.Title>
						<Card.Subtitle className="pb-2">Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
					</div>
					<div className="mt-auto d-flex flex-row justify-content-between align-items-center">
						<Card.Subtitle>Php {price}</Card.Subtitle>
						<div className="cart-error-container">
							{ !isLoggedIn ? <p className="cart-error">*please login</p> : null}
							<Button className="btn-addToCart btn-secondary" onClick={addToCart} disabled={isDisabled}>
								Add To Cart
							</Button>
						</div>
					</div>
				</Card.Body>
			</Card>
		</div>
	)
}

export default Item