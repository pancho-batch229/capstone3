import { AiOutlineShoppingCart } from "react-icons/ai"
import { Badge, Dropdown, Form,Button } from "react-bootstrap"
import { useState, useEffect, useContext } from "react"
import { FaUser } from 'react-icons/fa'
import { useGlobalContext } from "../../state/index"
import Swal from "sweetalert2"
import { useNavigate, Link } from "react-router-dom"

const Navbar = ({selectedPage, setSelectedPage}) => {
	const { user, setUser, unsetUser, openModal,isLoggedIn, setIsLoggedIn, cartCount, setCartCount, products, cartProducts } = useGlobalContext()

	const [isLogin, setIsLogin] = useState(true)
	const [error, setError] = useState("")
	const navigate = useNavigate()

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [confirmPass, setConfirmPass] = useState("")

 	const [isButtonDisabledLogin, setIsButtonDisabledLogin] = useState(true)
 	const [isButtonDisabledReg, setIsButtonDisabledReg] = useState(true)
	const [showModal, setShowModal] = useState(false)

	useEffect(() => {
		setError(password !== confirmPass ? "Password and Confirm Password do not match" : "")
		setIsButtonDisabledReg(!email || !password || !confirmPass || password !== confirmPass);
		setIsButtonDisabledLogin(!email || !password);
	}, [email, password, confirmPass])

	useEffect(() => {
		setError("")
	}, [email, password])

	const register = async (e) => {
		e.preventDefault()

		const response = await fetch("https://capston2-mangco.onrender.com/users/register", {
			method: "POST",
			body: JSON.stringify({ email, password }),
			headers: { "Content-Type": "application/json"}
		})
		const data = await response.json()
		if(!data) {
			setError("Email Already Exists!")
		} else {
			Swal.fire({
				confirmButtonColor: '#8f7840',
				color: '#8f7840',
				background: '#ecebeb',
      	title: "Successfully Registered",
      	icon: "success",
      	text: "You can now Log In using your registered Email."
      })
			setEmail("")
			setPassword("")
			setConfirmPass("")
		}		
	}


	const login = async (e) => {
		e.preventDefault()

		const response = await fetch("https://capston2-mangco.onrender.com/users/login", {
			method: "POST",
			body: JSON.stringify({ email, password }),
			headers: { "Content-Type": "application/json"},
		})
		
		const data = await response.json()	

		if(data.isAdmin) navigate('/admin')	
		if(data.access) {
			localStorage.setItem('email', email)	
			localStorage.setItem('isAdmin', data.result.isAdmin)
    		localStorage.setItem('token', data.access)
			setIsLoggedIn(true)		
			setUser({
				id: data.result._id,
				isAdmin: data.result.isAdmin,
				token: data.access
			})
			Swal.fire({
				confirmButtonColor: '#8f7840',
				color: '#8f7840',
				background: '#ecebeb',
		        title: "Successfully Logged In",
		        icon: "success",
		        text: `Welcome back ${email}`
		      })
		} else {
			setError("Invalid email or password")			
		}				
	}

	const logout = () => {
		unsetUser()
		setIsLoggedIn(false)
		setEmail("")
		setPassword("")
		setConfirmPass("")
		navigate('/')
		Swal.fire({
			confirmButtonColor: '#8f7840',
			color: '#8f7840',
			background: '#ecebeb',
      title: "Logging Out",
      icon: "success",
      text: `Thank you for shopping ${email}`
    })
	}

	const handleSubmit = async (e) => {
		if (isLogin) login(e)
		if (!isLogin) register(e)
	}

	const handleEmailInput = (e) => setEmail(e.target.value)
	const handlePwdInput = (e) => setPassword(e.target.value)
	const handleConfirmPwdInput = (e) => setConfirmPass(e.target.value)

	const errClass = error ? "error-msg" : "offscreen"



	return (
		<nav className="navbar">
			<div className="container d-flex justify-content-between align-items-center">
		
				<div className="d-flex align-items-center">
					<span className="h3 fw-bold mb-0 brand" onClick={() => navigate("/")}>Knives Out</span>
				</div>
				<div className="nav-menu d-flex align-items-center justify-content-between">					
					{!user.isAdmin && isLoggedIn && (
						<>
							<Link to="/products">Products</Link>
							<Link to="/order">Your Orders</Link>
							<div
								className="btn-icon"
								onClick={openModal}
							>
								Cart
								<Badge pill bg="danger" >{cartCount}</Badge>
							</div>
						</>
					)}
					<div>
						<Dropdown>
					    <Dropdown.Toggle 
					    	variant="btn-secondary" 
					    	className="dropdown-toggle" 
					    	id="dropdown-basic"
				    	>
					     	{isLoggedIn && user ? email : "Login/Register"}
					    </Dropdown.Toggle>
					    <Dropdown.Menu className="login-form">
					      {!isLoggedIn ? (
					        <>
						        <Form 
						          	className="p-3 form-size" 
						          	onSubmit={handleSubmit}
					          	>
						            <Form.Group controlId="formBasicEmail" className="pb-3">					            						            	
						            	<p className={errClass}>{error}</p>
						              <Form.Control 
						              	type="email" 
						              	placeholder="Enter email" 
						              	name="email" 
						              	value={email} 
						              	onChange={handleEmailInput} 
						              	required

						              />					        
						            </Form.Group>
						            <Form.Group className="pb-3" controlId="formBasicPassword">
						              <Form.Control 
						              	type="password" 
						              	placeholder="Password" 
						              	name="password" 
						              	value={password} 
						              	onChange={handlePwdInput} 
						              	required
					              	/>					            	
						            </Form.Group>
						            {!isLogin && (
						              <Form.Group 
						              	controlId="formBasicPassword" 
						              	className="pb-3"
					              	>
						                <Form.Control 
						                	type="password" 
						                	placeholder="Confirm Password" 
						                	name="password-confirm" 
						                	value={confirmPass} 
						                	onChange={handleConfirmPwdInput} 
						                	required
					                	/>
						              </Form.Group>
						            )}
						            <div className="d-flex flex-column">
						              {isLogin ? (
						                <Button 
						                	variant="login" 
						                	type="submit" 
						                	disabled={isButtonDisabledLogin}
					                	>
					                		Login
				                		</Button>
						              ) : (
						                <Button 
						                	variant="login" 
						                	type="submit" 
						                	disabled={isButtonDisabledReg}
					                	>
					                		Register
				                		</Button>
						              )}
						              <span className="login-register" onClick={() => setIsLogin(!isLogin)}>
						                {isLogin ? 'Create an account' : 'Already have an account'}
						              </span>
						            </div>
						        </Form>
					        </>
					      ) : (
					        <>
					          <Dropdown.Item onClick={logout}>Logout</Dropdown.Item>
					        </>
					      )}
					    </Dropdown.Menu>
					  </Dropdown>
					</div>
				</div>
			</div>
		</nav>
	)
}

export default Navbar