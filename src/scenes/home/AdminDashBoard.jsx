import React from 'react'
import { Button, Table, Form, DropdownButton, Dropdown } from "react-bootstrap"
import { useState, useEffect } from "react"
import { useGlobalContext } from "../../state/index"

 

const AdminDashBoard = () => {
	const { openModalProduct, products, setProducts, user, isUpdate, setIsUpdate , setSelectedProductId} = useGlobalContext()



	const fetchProducts = async () => {
		const res = await fetch("https://capston2-mangco.onrender.com/products/all/admin")
		const data = await res.json()

		setProducts(data)
	}

	const handleDropdownChange = async (productId, isActive) => {
    const updatedProduct = await archiveProduct(productId, isActive)
    setProducts(prevProducts => prevProducts.map(product => product._id === productId ? { ...product, isActive: updatedProduct.isActive } : product))
  }

	const archiveProduct = async (productId, isActive) => {
		const res = await fetch(`https://capston2-mangco.onrender.com/products/${productId}/archive`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				isActive
			})
		})

		const archivedProduct = await res.json()
		return archivedProduct
	}
	useEffect(() => {
		fetchProducts()
	}, [products])




	return (
		<div className="admin-bg">
			<div className="container-fluid table-container">
				<div className="admin-buttons">
					<Button className="mb-4 btn-secondary btn-login" onClick={() => {openModalProduct(); setIsUpdate(false)}}>Create Product</Button>
				</div>
				<Table striped bordered hover variant="dark">
			      <thead>
			        <tr>
			          <th>Product Name</th>
			          <th>Description</th>
			          <th>Price</th>
			          <th>Created At</th>
			          <th>Updated At</th>
			          <th>Archive</th>
			          <th>Update</th>
			        </tr>
			      </thead>
			      <tbody>
			        {products.map((product) => (
			        	<tr key={product._id}>			        		
			        		<td>{product.name}</td>
			        		<td>{product.description}</td>
			        		<td>{product.price}</td>
			        		<td>
			        			{
			        				new Date(product.createdAt).toLocaleDateString("en-US", {
			        					 	year: "numeric",
										      month: "2-digit",
										      day: "2-digit"
			        				})
			        			}			        			
			        		</td>
			        		<td>
			        			{
			        				new Date(product.createdAt).toLocaleDateString("en-US", {
			        					 	year: "numeric",
										      month: "2-digit",
										      day: "2-digit"
			        				})
			        			}
			        		</td>
			        		<td>
			        			<DropdownButton id="dropdown-basic-button" className="btn-secondary" title={product.isActive ? "Active" : "Archived"}>
				                    <Dropdown.Item onClick={() => handleDropdownChange(product._id, true)}>Active</Dropdown.Item>
				                    <Dropdown.Item onClick={() => handleDropdownChange(product._id, false)}>Archived</Dropdown.Item>
			                  	</DropdownButton>
			        		</td>
			        		<td>
			        			<Button 
			        				variant="btn btn-update btn-secondary" 
			        				onClick={() => {
			        					setIsUpdate(true); 
			        					openModalProduct();
			        					setSelectedProductId(product._id)
			        				}}>Update</Button>

			        		</td>
			        	</tr>	        	
			        ))}
			      </tbody>
			    </Table>
			</div>
		</div>
	)
}

export default AdminDashBoard