import { Button } from "react-bootstrap"
import ShoppingList from "./ShoppingList"
import AdminDashBoard from "./AdminDashBoard"
import { useGlobalContext } from "../../state/index"
import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"

const Home = () => {
	const { user,showAdminDash, setShowAdminDash, isLoggedIn, setIsLoggedIn } = useGlobalContext()
	const [selectedPage, setSelectedPage] = useState("home")
	const navigate = useNavigate()

	useEffect(() => {
		if(user.isAdmin) {
			setShowAdminDash(true)
		} else {
			setShowAdminDash(false)
		}
	},[user])

	return (
		<div>
			
				{ isLoggedIn && showAdminDash ? (
						<AdminDashBoard />						
					) : (
						<div className="home">
							<section className="container" id="home">
								<div className="hero">
									<h1>Welcome to the Best Kitchen Knives Shop!</h1>
									<p>High-quality kitchen knives for all needs. Efficient and enjoyable cooking experience. Durable materials.</p>
									<ul>
										<li>Free Shipping</li>
										<li>100% Satisfaction Guarantee</li>
										<li>30-day Returns</li>
										<li>Wide Selection of Knives for All Cooking Needs</li>
										<li>Affordable Prices</li>
									</ul>
									<Button onClick={() => navigate('/products')}>Shop Now</Button>
								</div>
							</section>
							<ShoppingList setSelectedPage={setSelectedPage}/>
						</div>
					)
				}
						
		</div>
	)
}

export default Home