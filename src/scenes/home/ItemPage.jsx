import { useGlobalContext } from "../../state/index"
import { useState, useEffect } from "react"
import { BiMinusCircle, BiPlusCircle } from "react-icons/bi"
import { Button, Image } from "react-bootstrap"
import Swal from "sweetalert2"



const ItemPage = () => {
	const { selectedProductId, isLoggedIn, user } = useGlobalContext()
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [count , setCount] = useState(1)

	

	useEffect(() => {
		const fetchProduct = async () => {
			const response = await fetch(`https://capston2-mangco.onrender.com/products/${selectedProductId}`, {
				method: "GET",
				headers: { 'Content-Type': 'application/json'}
			})
			const data = await response.json()
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		}
		fetchProduct()
	}, [selectedProductId])

	const addToCart = async () => {
		const response = await fetch(`https://capston2-mangco.onrender.com/carts/${user.id}`, {
			method: "POST",
			body: JSON.stringify({
				productId: selectedProductId,
				quantity: count,
			}),
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			}
		})

		const data = await response.json()
		Swal.fire({
			position: 'top-end',
			timer: 1500,
			showConfirmButton: false,
			color: '#8f7840',
			background: '#ecebeb',
	    	title: "Success",
	    	icon: "success",
	    	text: `${name} successfully added to cart` 
	    })
	}


	return (
		<div className="bg-itempage">
			<div className="container d-flex justify-content-between align-items-center">
				<div className="image-container-item">
					<Image width="600px" src={`../../assets/products/${selectedProductId}.png`} fluid />
				</div>
				<div className="product-details px-5">
					<div>
						<h1>{name}</h1>
						<p>{description}</p>
						<h4>Php {price}</h4>
					</div>
					{
						isLoggedIn &&
						<>
							<div className="mt-4 d-flex align-items-center count-buttons">
								<BiPlusCircle onClick={() => setCount(count + 1)} className="plus-count" size={40}/>			
								{count}
								<BiMinusCircle onClick={() => count > 1 && 	setCount(count - 1)} className="plus-count" size={40}/>	
							</div>
							<Button className="btn-addToCart mt-4 btn-secondary" onClick={addToCart}>
								Add To Cart
							</Button>
						</>
					}
				</div>
			</div>
		</div>
	)
}

export default ItemPage