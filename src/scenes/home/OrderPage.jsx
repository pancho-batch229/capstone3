import { useEffect, useState } from "react"
import { useGlobalContext } from "../../state/index"
import { Table } from "react-bootstrap"

const OrderPage = () => {
	const { user } = useGlobalContext()
	const [orders, setOrders] = useState()

	const fetchOrders = async () => {
		const response = await fetch(`https://capston2-mangco.onrender.com/orders/user/${user.id}`, {
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			}
		})

		const data = await response.json()
		setOrders(data)	
	}

	useEffect(() => {
		fetchOrders()
	}, [])

	return (
		<div className="order-page">
			<div className="container">
				<h1 className="pt-4 mb-4">Your Orders</h1>
				{ orders ? (
						<Table striped bordered hover variant="dark">
				      <thead>
				        <tr>
				          <th>Order ID</th>
				          <th>Total Amount</th>
				          <th>Purhcased On</th>
				        </tr>
				      </thead>
				      <tbody>
				      	{ orders.map(order => (
									<tr key={order._id}>
										<td>{order._id}</td>
										<td>{order.totalAmount}</td>
										<td>
											{
				        				new Date(order.purhasedOn).toLocaleDateString("en-US", {
			        					 	year: "numeric",
										      month: "2-digit",
										      day: "2-digit"
				        				})
				        			}
										</td>
									</tr>
								))}
				      </tbody>
			      </Table>
					) : (
						<div>
							<h3>Loading ... </h3>
						</div>
					)
				}					
			</div>
		</div>
	)
}

export default OrderPage