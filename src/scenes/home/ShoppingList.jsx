import Item from "../../components/Item"
import { useState, useEffect } from "react"
import { Row, Col } from "react-bootstrap"
import { useParams } from "react-router-dom"
import PropagateLoader from 'react-spinners/PropagateLoader'


const ShoppingList = ({setSelectedPage}) => { 
	const [products, setProducts] = useState([])
	const [loading, setLoading] = useState(true)

	const fetchProducts = async () => {
		setLoading(true)

		const res = await fetch(`https://capston2-mangco.onrender.com/products/all`)
		const data = await res.json()

		setProducts(data.map(prod => (
			<Col key={prod._id} xs={12} md={6} lg={4}>
				<Item productProps={prod}/>
			</Col>
		)))

		setLoading(false)
	}

	useEffect(() => {
		fetchProducts()
	}, [])

	return (
		<section className="shopping-list" id="products">
			<div className="container mt-4">
				<h2 className="mb-4">Our Products</h2>
				<Row>
					{ loading ? (
							<div className="loader">							
								<PropagateLoader color="#8f7840" />
							</div>
						) : (
							products
						)
					}
				</Row>
			</div>
		</section>
	)
}

export default ShoppingList
