import { Link } from "react-router-dom"


const NotFound = () => {
	return (
		<div className="not-found">
			<div>
				<h1>NOT FOUND</h1>
				<p>Go back to <Link to="/">homepage</Link></p>
			</div>
		</div>
	)
}

export default NotFound