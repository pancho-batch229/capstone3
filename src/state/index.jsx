import React, { useContext , useEffect, useState} from "react";

const UserContext = React.createContext()


const UserProvider = ({ children }) => {

	const [showModal, setShowModal] = useState(false);
	const [showAdminDash, setShowAdminDash] = useState(false)
	const [isLoggedIn, setIsLoggedIn] = useState(false);
	const [showModalProduct, setShowModalProduct] = useState(false)
	const [isUpdate, setIsUpdate] = useState(false)
	const [selectedProductId, setSelectedProductId] = useState("")
	

	const [ cartCount, setCartCount ] = useState(0)
	const [ cartProducts, setCartProducts ] = useState(null)

	const [products, setProducts] = useState([])

	const [user, setUser] = useState({
	    id: null,
	    isAdmin: null
	})

	const closeModal = () => {
		setShowModal(false)
	}

	const openModal = () => {
		setShowModal(true)
	}

	const openModalProduct = () => {
		setShowModalProduct(true)
	}
	const closeModalProduct = () => {
		setShowModalProduct(false)
	}

	const getUserFromStorage = () => {
		localStorage.getItem('isAdmin')
	}

	

	const unsetUser = () => {
		localStorage.clear()
	}


	

	return <UserContext.Provider value={{ cartProducts, setCartProducts, cartCount, setCartCount, getUserFromStorage, user, setUser, unsetUser, openModal, showModal, closeModal, showAdminDash, setShowAdminDash,isLoggedIn, setIsLoggedIn,closeModalProduct, openModalProduct, showModalProduct, products, setProducts, setShowModalProduct, isUpdate, setIsUpdate, selectedProductId, setSelectedProductId }}> 
		{children} 
	</UserContext.Provider>
		
	
}

export const useGlobalContext = () => {
	return useContext(UserContext)
}

export { UserContext, UserProvider }